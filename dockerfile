FROM maven:3.9.5-sapmachine-17
USER root
RUN apt-get update
RUN apt install -y pandoc xsltproc docbook docbook-xsl

CMD ["/bin/bash"]
